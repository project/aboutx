<?php
/**
 * @file
 * Custom theme implementation to display a single Drupal page.
 */
?>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only"><?php print t('Toggle navigation') ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php if($site_name) : ?>
                <?php print $logo ?>
                <a class="navbar-brand" href="#page-top"><?php print $site_name; ?></a>
            <?php endif;?>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <?php print $site_slogon; ?>
        </div>
    </div>
    <!-- /.container-fluid -->
</nav>

<!-- Header -->
<?php if($page['header']) : ?>
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <?php print render($page['header']); ?>
            </div>
        </div>
    </div>
</header>
<?php endif; ?>

<!-- Portfolio Grid Section -->
<?php if ($page['content_header']) : ?>
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <?php print render($page['content_header']); ?>
                    <hr class="star-primary">
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<!-- About Section -->
<?php if ($page['content']) : ?>
    <section class="success" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <?php print render($page['content']); ?>
                    <hr class="star-light">
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<!-- Contact Section -->
<?php if ($page['content_bottom']) : ?>
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <?php print render($page['content_bottom']); ?>
                    <hr class="star-primary">
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<!-- Footer -->
<footer class="text-center">
    <?php if($page['footer_above']) : ?>
    <div class="footer-above">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <?php print render($page['footer_above']); ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
    <?php if($page['footer_below']) : ?>
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <?php print render($page['footer_below']); ?>
                </div>
            </div>
        </div>
    </div>
    <?php endif; ?>
</footer>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-top page-scroll visible-xs visible-sm">
    <a class="btn btn-primary" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>

<!-- Portfolio Modals -->

