<?php
/**
 * @file
 * Custom theme implementation to display a single Drupal page.
 */
?>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->


        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only"><?php print t('Toggle navigation') ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php if($name) : ?>
                <a class="navbar-brand" href="#page-top"><?php print $name; ?></a>
            <?php endif;?>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden">
                    <a href="#page-top"></a>
                </li>
                <?php if ($show_portfolio) : ?>
                    <li class="page-scroll">
                        <a href="#portfolio"><?php print isset($portfolio_link_text) ? $portfolio_link_text : t('Portfolio'); ?></a>
                    </li>
                <?php endif; ?>
                <?php if ($show_about) : ?>
                    <li class="page-scroll">
                        <a href="#about"><?php print ($about_link_text) ? $about_link_text : t('About'); ?></a>
                    </li>
                <?php endif; ?>
                <?php if ($show_contact) : ?>
                    <li class="page-scroll">
                        <a href="#contact"><?php print ($contact_link_text) ? $contact_link_text : t('Contact'); ?></a>
                    </li>
                <?php endif; ?>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>

<!-- Header -->
<header>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <img class="img-responsive profile-img" src="<?php print file_create_url($aboutx_profile_picture->uri); ?>" alt="">
                <div class="intro-text">
                    <?php if($name) : ?>
                        <span class="name"><?php print $name; ?></span>
                    <?php endif; ?>
                    <?php if($description) : ?>
                        <hr class="star-light">
                        <span class="skills"><?php print $description ?></span>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Portfolio Grid Section -->
<?php if ($show_portfolio) : ?>
    <section id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><?php print $portfolio_link_text; ?></h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <?php for($i = 1; $i <= $number_of_project; $i++) : ?>
                    <div class="col-sm-4 portfolio-item">
                        <a href="#portfolioModal<?php print $i; ?>" class="portfolio-link" data-toggle="modal">
                            <div class="caption">
                                <div class="caption-content">
                                    <i class="fa fa-search-plus fa-3x"></i>
                                </div>
                            </div>
                            <?php $product_image = file_create_url($aboutx_project_image[$i]->uri); ?>
                            <img src="<?php print $product_image ?>" class="img-responsive" alt="">
                        </a>
                    </div>
                    <div class="portfolio-modal modal fade" id="portfolioModal<?php print $i ?>" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-content">
                            <div class="close-modal" data-dismiss="modal">
                                <div class="lr">
                                    <div class="rl">
                                    </div>
                                </div>
                            </div>
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="modal-body">
                                            <h2><?php print theme_get_setting('project_title_'.$i); ?></h2>
                                            <hr class="star-primary">
                                            <img src="<?php print $product_image ?>" class="img-responsive img-centered" alt="">
                                            <p><?php print theme_get_setting('project_description_'.$i); ?></p>
                                            <ul class="list-inline item-details">
                                                <li> <?php print t('Client:') ?>
                                                    <strong><a href="http://startbootstrap.com"><?php print theme_get_setting('project_client_'.$i); ?></a>
                                                    </strong>
                                                </li>
                                                <li><?php print t('Date:') ?>
                                                    <strong><?php print theme_get_setting('project_date_'.$i); ?></a>
                                                    </strong>
                                                </li>
                                                <li><?php print t('Service:') ?>
                                                    <strong><?php print theme_get_setting('project_service_'.$i); ?></a>
                                                    </strong>
                                                </li>
                                            </ul>
                                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i><?php print t('Close') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endfor ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<!-- About Section -->
<?php if($show_about) : ?>
    <section class="success" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><?php print isset($about_link_text) ? $about_link_text : t('About') ?></h2>
                    <hr class="star-light">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-lg-offset-2">
                    <p><?php print $about_left; ?></p>
                </div>
                <div class="col-lg-4">
                    <p><?php print $about_right ?></p>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<!-- Contact Section -->
<?php if($show_contact) : ?>
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><?php print isset($contact_link_text) ? $contact_link_text  : t('Contact Me') ?></h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-center">
                    <?php print $contact; ?>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>

<!-- Footer -->
<footer class="text-center">
    <div class="footer-above">
        <div class="container">
            <div class="row">
                <?php if($location) : ?>
                    <div class="footer-col col-md-4">
                        <h3><?php print t('Location'); ?> </h3>
                        <p><?php print $location ?></p>
                    </div>
                <?php endif; ?>
                <div class="footer-col col-md-4">
                    <h3><?php print t('Around the Web') ?></h3>
                    <ul class="list-inline">
                        <?php if($facebook) : ?>
                            <li>
                                <a href="<?php print $facebook ?>" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if($googleplus) : ?>
                            <li>
                                <a href="<?php print $googleplus ?>" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if($twitter) : ?>
                            <li>
                                <a href="<?php print $twitter ?>" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if($linkedin) : ?>
                            <li>
                                <a href="<?php print $linkedin ?>" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                            </li>
                        <?php endif; ?>
                        <?php if($dribble) : ?>
                            <li>
                                <a href="<?php print $dribble ?>" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="footer-col col-md-4">
                    <h3><?print t('About me'); ?></h3>
                    <p><?php print $about_left; ?></p>
                    <p><?php print $about_right ?></p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-below">
        <div class="container">
            <div class="row">
                <?php if($page['footer']) :
                    print render($page['footer']); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</footer>

<!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
<div class="scroll-top page-scroll visible-xs visible-sm">
    <a class="btn btn-primary" href="#page-top">
        <i class="fa fa-chevron-up"></i>
    </a>
</div>

<!-- Portfolio Modals -->

