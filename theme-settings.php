<?php
/**
* @file
* Theme setting callbacks for the html5_simplified theme.
*
* Implements hook_form_FORM_ID_alter().
*
* @param $form
*   The form.
* @param $form_state
*   The form state.
*/
function aboutx_form_system_theme_settings_alter(&$form, &$form_state) {
$form['top'] = array(
'#type' => 'fieldset',
'#title' => t('Top section'),
    '#collapsible'=>TRUE,
    '#collapsed' => TRUE,
);
    $form['portfolio'] = array(
        '#type' => 'fieldset',
        '#title' => t('Portfolio'),
        '#collapsible'=>TRUE,
        '#collapsed' => TRUE,
    );
    $form['about'] = array(
        '#type' => 'fieldset',
        '#title' => t('About Me'),
        '#collapsible'=>TRUE,
        '#collapsed' => TRUE,
    );
    $form['contact'] = array(
        '#type' => 'fieldset',
        '#title' => t('Contact'),
        '#collapsible'=>TRUE,
        '#collapsed' => TRUE,
    );
    $form['social'] = array(
        '#type' => 'fieldset',
        '#title' => t('Social'),
        '#collapsible'=>TRUE,
        '#collapsed' => TRUE,
    );
    $form['misc'] = array(
        '#type' => 'fieldset',
        '#title' => t('Mics'),
        '#collapsible'=>TRUE,
        '#collapsed' => TRUE,
    );

           $form['top']['image'] = array(
               '#type' => 'managed_file',
               '#title' => t('Upload your profile picture.'),
               '#default_value' => theme_get_setting('image', 'aboutx'),
               '#upload_location' => 'public://aboutx/profile-picture/',
               '#upload_validators' => array(
                   'file_validate_extensions' => array('gif png jpg jpeg'),
                   'file_validate_size' => array(0.6*1024*1024),
               ),
           );
$form['top']['name'] = array(
'#type' => 'textfield',
'#title' => t('Enter your name'),
'#default_value' => theme_get_setting('name', 'aboutx'),
);
    $form['top']['description'] = array(
        '#type' => 'textfield',
        '#title' => t('Enter your desigination / role'),
        '#default_value' => theme_get_setting('description', 'aboutx'),
        '#description'=>t('Web Developer - Graphic Artist - User Experience Designer'),
    );
    $form['portfolio']['show_portfolio'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show project page'),
        '#default_value' => theme_get_setting('show_portfolio', 'aboutx')
    );
    $form['portfolio']['portfolio_link_text'] = array(
        '#type' => 'textfield',
        '#title' => t('Portfolio link text'),
        '#default_value' => theme_get_setting('portfolio_link_text', 'aboutx')
    );
    $number_of_project  = theme_get_setting('number_of_project', 'aboutx');
        $form['portfolio']['number_of_project'] = array(
            '#type' => 'textfield',
            '#title' => t('Show number of project'),
            '#default_value' => $number_of_project
        );
    if(empty($number_of_project)) {
        $number_of_project = 4;
    }
    for($counter=1; $counter <= $number_of_project; $counter++) {
        $form['portfolio']['project_detail_' . $counter] = array(
            '#type' => 'fieldset',
            '#title' => t('Project Details'),
            '#collapsible' => TRUE,
            '#collapsed' => TRUE,
        );
        $form['portfolio']['project_detail_'.$counter]['project_title_'.$counter] = array(
            '#type' => 'textfield',
            '#title' => t('Project title'),
            '#default_value' => theme_get_setting('number_of_project', 'aboutx'),
        );
        $form['portfolio']['project_detail_'.$counter]['project_description_'.$counter] = array(
            '#type' => 'textarea',
            '#title' => t('Project description'),
            '#default_value' => theme_get_setting('number_of_project', 'aboutx'),
        );
        $form['portfolio']['project_detail_'.$counter]['project_image_'.$counter] = array(
            '#type' => 'managed_file',
            '#title' => t('Project image.'),
            '#default_value' => theme_get_setting('project_image_'.$counter, 'aboutx'),
            '#upload_location' => 'public://aboutx/project-picture/',
            '#upload_validators' => array(
                'file_validate_extensions' => array('gif png jpg jpeg'),
                'file_validate_size' => array(0.6*1024*1024),
            ),
        );
        $form['portfolio']['project_detail_'.$counter]['project_client_'.$counter] = array(
            '#type' => 'textfield',
            '#title' => t('Project client'),
            '#default_value' => theme_get_setting('project_client_'.$counter, 'aboutx'),
        );
        $form['portfolio']['project_detail_'.$counter]['project_date_'.$counter] = array(
            '#type' => 'textfield',
            '#title' => t('Project date'),
            '#default_value' => theme_get_setting('project_date_'.$counter, 'aboutx'),
        );
        $form['portfolio']['project_detail_'.$counter]['project_service_'.$counter] = array(
            '#type' => 'textfield',
            '#title' => t('Project service'),
            '#default_value' => theme_get_setting('project_service_'.$counter, 'aboutx'),
        );
    }
    $form['about']['show_about'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show about page'),
        '#default_value' => theme_get_setting('show_about', 'aboutx')
    );
    $form['about']['about_link_text'] = array(
        '#type' => 'textfield',
        '#title' => t('About link text'),
        '#default_value' => theme_get_setting('about_link_text', 'aboutx')
    );
    $form['about']['about_left'] = array(
        '#type' => 'textarea',
        '#title' => t('About Left'),
        '#default_value' => theme_get_setting('about_left', 'aboutx')
    );
    $form['about']['about_right'] = array(
        '#type' => 'textarea',
        '#title' => t('About right'),
        '#default_value' => theme_get_setting('about_right','aboutx')
    );
    $form['contact']['show_contact'] = array(
        '#type' => 'checkbox',
        '#title' => t('Show contact page'),
        '#default_value' => theme_get_setting('show_contact', 'aboutx')
    );
    $form['contact']['contact_link_text'] = array(
        '#type' => 'textfield',
        '#title' => t('Contact link text'),
        '#default_value' => theme_get_setting('contact_link_text', 'aboutx')
    );
    $form['contact']['contact'] = array(
        '#type' => 'textarea',
        '#title' => t('Contact'),
        '#default_value' => theme_get_setting('contact', 'aboutx')
    );

    $form['social']['facebook'] = array(
        '#type' => 'textfield',
        '#title' => t('Facebook'),
        '#default_value' => theme_get_setting('facebook', 'aboutx')
    );
    $form['social']['twitter'] = array(
        '#type' => 'textfield',
        '#title' => t('Twitter'),
        '#default_value' => theme_get_setting('twitter', 'aboutx')
    );
    $form['social']['googleplush'] = array(
        '#type' => 'textfield',
        '#title' => t('Google plus'),
        '#default_value' => theme_get_setting('googleplush', 'aboutx')
    );
    $form['social']['linkedin'] = array(
        '#type' => 'textfield',
        '#title' => t('LinkedIn'),
        '#default_value' => theme_get_setting('linkedin', 'aboutx')
    );
    $form['social']['dribble'] = array(
        '#type' => 'textfield',
        '#title' => t('Dribble'),
        '#default_value' => theme_get_setting('dribble', 'aboutx')
    );
    $form['misc']['location'] = array(
        '#type' => 'textarea',
        '#title' => t('Location'),
        '#default_value' => theme_get_setting('location', 'aboutx')
    );

}
