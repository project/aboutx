<?php

/**
 * @file
 * Provides preprocess logic and other functionality for theming.
 */

/**
 * Implement hook_preprocess_html().
 */
function aboutx_preprocess_html(&$var) {
//Nothing to add here
}

/**
 * Implement hook_preprocess_page().
 */
function aboutx_preprocess_page(&$var) {
    $aboutx_settings = variable_get('theme_aboutx_settings',array());
    foreach(array_keys($aboutx_settings) as $settings) {
        $var[$settings] = theme_get_setting($settings);
    }
    for($i=1; $i <= $var['number_of_project']; $i++ ) {
        if(isset($var['project_image_'.$i])) {
            $var['aboutx_project_image'][$i] = file_load($var['project_image_'.$i]);
        }
    }
    if(isset($var['image'])) {
        $var['aboutx_profile_picture'] = file_load($var['image']);
    }
}

function aboutx_js_alter(&$javascript) {
    $javascript['misc/jquery.js']['data'] =         drupal_get_path('theme', 'aboutx') . '/js//jquery.js';
}